
/*
 *  Copyright 2021 籽籮籮 籵籮籮籯类籲籷籰
 *  (C) Universitaet Passau 1986-1991
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "splay-tree.h"
#include "main.h"
#include "hier.h"
#include "pos.h"
#include "pos2.h"

/* position in parts of graph at each step */
void
improve_positions2 (struct gml_graph *g)
{

  struct gml_nlist *gnl = NULL;

  /* copy the rel(x,y) pos into abs(x,y) and modify the absx pos here */
  gnl = g->nodelist;

  while (gnl)
    {
      gnl->node->absx = gnl->node->relx;
      gnl->node->absy = gnl->node->rely;
      gnl->node->finx = 0;
      gnl->node->finy = 0;
      gnl = gnl->next;
    }

  printf ("%s(): run another positioning algorithm here\n", __func__);
 /* keep it as simple as possible but not simpler then that said einstein. e=mc2 */
  /* relative positions are in (absx,abxy)
   * put result in (finx,finy)
   * see also GNU GPL Free Software or other Open Source:
   * see also igraph c library sugiyama.c GNU GPL
   * see also d3 dagre javascript lib MIT
   * see also elk java lib Open Source
   * see also ogdf c++ lib GNU GPL
   * see also tulip software GNU GPL
   * see also matthian stallman mincrossings GNU GPL
   * see also russian mipt-vis and idcv GNU GPL Version 3+
   * see also multiple pdf's about the errors in the bk algorithm
   * the bk algorithm needs updates to handle nodes with variable (x,y) sizes
   * also consider other algorithms
   * indicate type of edges and the sub parts
   * determine 4 possible positionings
   * positioning the sub parts
   */

  return;
}

/* end */
