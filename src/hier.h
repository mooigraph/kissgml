
/*
 *  Copyright 2021 籽籮籮 籵籮籮籯类籲籷籰
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#ifndef HIER_H
#define HIER_H 1

extern int yydebug;

/* positioning mode */
extern int posmode;

/* parser messages */
extern char parsermessage[256];

/* max y level */
extern int maxlevel;

/* number of levels */
extern int nlevels;

/* number of nodes at level */
extern int *nnodes_of_level;

/* number of edges between level n and n+1 */
extern int *nume;

/* level with most nodes */
extern int widestlevel;

/* number of nodes at widest level */
extern int widestnnodes;

/* max x of end drawing */
extern int maxx;

/* max y of end drawing */
extern int maxy;

/* width of positions */
extern int *wpos;

/* lists per pos. */
extern struct gml_nlist **posnodes;

/* height of levels */
extern int *hpos;

/* lists per level */
extern struct gml_nlist **levelnodes;

/* calculate edge connections */
extern void edgeconnections (struct gml_graph *g);

/* clear self-edges node list */
extern void clear_selfedgesnodelist (struct gml_graph *g);

/* */
extern void clear_nume (void);

/* clear arrays in/out edges */
extern void clear_ioedges (struct gml_graph *g);

/* clear the s/t list of a node */
extern void clear_stlist (struct gml_node *node);

/* clear the s/t list of all nodes */
extern void clear_stlist_all (struct gml_graph *g);

/* free nodelist and nodes */
extern void clear_rawnodelist (struct gml_graph *g);

/* free edgelist */
extern void clear_rawedgelist (struct gml_graph *g);

/* free nodelist and nodes */
extern void clear_nodelist (struct gml_graph *g);

/* free single nodelist */
extern void clear_singlenodelist (struct gml_graph *g);

/* free edgelist */
extern void clear_edgelist (struct gml_graph *g);

/* rebuild nodes st lists */
extern void make_stlist (struct gml_graph *g);

/* delete edge */
extern void del_edge (struct gml_graph *g, struct gml_elist *edgeel);

/* splits edgelabel edges into node->label->node */
extern void edgelabels (struct gml_graph *g);

/* break cycle reversing last edge in it */
extern void decycle (struct gml_node *n, int num);

/* break cycles in the graph */
extern void uncycle (struct gml_graph *g);

/* all edges downwards */
extern void edgesdownwards (struct gml_graph *g);

/* set rel. y level of all nodes */
extern void ylevels (struct gml_graph *g);

/* dfs check again and revers if needed */
extern void edgelen (struct gml_graph *g);

/* try to find shorter edges */
extern void shorteredges (struct gml_graph *g);

/* doublespace the vertical levels */
extern void doublespacey (struct gml_graph *g);

/* split longer edges */
extern void splitedges (struct gml_graph *g);

/* create level node count data */
extern void nodecounts (struct gml_graph *g);

/* */
extern void add_new_node (struct gml_graph *g, int foundid, char *nodelabel,
			  int ncolor, struct gml_rl *rlabel);

/* */
extern void add_new_dummynode (struct gml_graph *g, int foundid);

/* */
extern void
add_new_edge (struct gml_graph *g, int foundsource, int foundtarget,
	      char *elabel, int ecolor, int style, char *fcompass,
	      char *tcompass);

/* */
extern void add_new_dummyedge (struct gml_graph *g, int foundsource,
			       int foundtarget, int reversed, int ecolor,
			       int style, char *fcompass, char *tcompass);

/* lists per position */
extern void make_posnodes (struct gml_graph *g);

/* clear pos. nodes lists */
extern void clear_posnodes (struct gml_graph *g);

/* lists per level */
extern void make_levelnodes (struct gml_graph *g);

/* clear level nodes lists */
extern void clear_levelnodes (struct gml_graph *g);

/* clear list of startnodes */
extern void clear_startnodes (struct gml_graph *g);

/* clear record labels */
extern void clear_rlabel (struct gml_graph *g);

#endif

/* end */
