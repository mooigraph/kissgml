# kissgml
  
Keep It Simple Stupid GNU/Linux only GTK+ program usable as a GML (Graph Markup Language) viewer  
  
This uses few parts of the GNU GPL sources of graphlet and the GNU GCC Compiler GNU GPL Version 3+  
This can be compiled for use with GTK-2 or GTK-3  
In the data directory are few examples  
To Compile:  
./autogen.sh  
./configure  
make  
make cleaner
  
Copyright Bill Struik
  
SPDX-License-Identifier: GPL-3.0+  
License-Filename: LICENSE  
  
